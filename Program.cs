﻿using System;
using System.Collections.Generic;

namespace laboratorio6
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IEstadoBinario> dispositivos = new List<IEstadoBinario>();
            dispositivos.Add(new Lampada(110,60));
            dispositivos.Add(new Lampada(110,100));
            dispositivos.Add(new TermometroEletrico());
            foreach(var disp in dispositivos)
            {
                Console.WriteLine(disp.Estado);
            }
            dispositivos[1].Ligar();
            foreach(var disp in dispositivos)
            {
                Console.WriteLine(disp.Estado);
            }
        }
    }
}
