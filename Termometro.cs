namespace laboratorio6
{
    public class Termometro
    {
        private decimal temperatura;
        public virtual decimal Temperatura
        {
            get {return temperatura;}
        }
        public virtual void Aumentar(decimal t)
        {
            temperatura += t;
        }
        public virtual void Dimininuir(decimal t)
        {
            temperatura -= t;
        }
    }
}