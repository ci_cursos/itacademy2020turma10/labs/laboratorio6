namespace laboratorio6
{
    public class TermometroEletrico : Termometro, IEstadoBinario
    {
        private bool ligado;

        public EstadoBinario Estado
        {
            get
            {
                if (ligado)
                {
                    return EstadoBinario.Ligado;
                }
                return EstadoBinario.Desligado;
            }
        }
        public void Desligar()
        {
            ligado = false;
        }

        public void Ligar()
        {
            ligado = true;
        }
    }
}